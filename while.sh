#!/bin/bash
clear
while [ true ]
do
	echo "Escolha uma opção"
	echo "1-Usuarios Logados"
	echo "2-Diretorio Atual"
	echo "3-Data Atual"
	echo "4-Lista Usuarios do Sistema"
	echo "5-Uptime da Estação"
	echo "6-Sair"
	read opcao

case $opcao in 
	1) echo 'Usuarios Logados:' && users;;
	2) echo 'Diretorio Atual:' && pwd;;
	3) echo 'Data Atual:' && date;;
	4) echo 'Memoria:' && free;;
	5) echo 'Usuarios dos Sistemas:' && cat /etc/passwd | awk -F: '{print $1'};; 
	6) break;;
esac
	echo "Voce Deseja sair(S\N)"
        read opcao2
	case $opcao2 in
	     s) break;;
        esac
done		 
